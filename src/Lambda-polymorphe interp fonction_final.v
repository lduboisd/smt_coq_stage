Require Import Nat.
Set Keep Proof Equalities.
Require Import List.
Require Import Arith.
Import ListNotations. 
Require Import Omega.

Import EqNotations.


(* Code de la version interprétation, fonctions d'interprétations, quelques lemmes
intermédiaires et énoncé du théorème principal *)


(* n-ième élément d'une liste avec possibilité d'erreur quand la liste est trop longue *)
Fixpoint nth_error {A: Type} (l: list A) (n: nat) {struct l} := match l, n with 
| nil, _ => None
| x::l', 0 => Some x
| x::l', (S i) => nth_error l' i
end.


Section Polymorphic.

Inductive PType : Type :=
| PVar : nat -> PType (* variable de type, implicitement quantifiée universellement en tête de formule *)
| PNat : PType
| Pa : PType -> PType -> PType
| PBool : PType
.


Inductive PTerm : Type :=
| PTermVar : nat -> PTerm 
| PLam : PType -> PTerm -> PTerm (* indices de De Brujin *) 
| PApp : PTerm -> PTerm -> PTerm
| PAnd : PTerm -> PTerm -> PTerm
| PNeg : PTerm -> PTerm
| PTrue : PTerm
| PFalse : PTerm
.

(* vérifie si deux types sont égaux *)

Fixpoint synt_eq_typeP (A B : PType) := match (A, B) with
| (PNat, PNat) => true
| (PVar i, PVar j) => i =? j
| (Pa X Y, Pa U V) => andb (synt_eq_typeP  X U) (synt_eq_typeP X V)
| (PBool, PBool) => true
| (_, _) => false
end.



(* à partir d'une liste de types pour les variables, 
calcule le type d'un terme *)

Fixpoint inferP' (c : list PType) (t:PTerm) : option PType :=
match t with
| PTermVar i => nth_error c i
| PTrue => Some PBool
| PFalse => Some PBool
| PNeg x => match inferP' c x with
        | Some PBool => Some PBool
        | _ => None
      end
| PAnd u v => match inferP' c u with 
         | Some PBool => match inferP' c v with 
                | Some PBool => Some PBool
                | _ => None
end
        | _ => None
end
| PLam  A u =>
match inferP' c u with
| Some B => Some (Pa A B)
| _ => None
end
| PApp u v =>
match inferP' c u, inferP' c v with
| Some (Pa A B), Some C =>
if synt_eq_typeP A C then Some B else None
| _, _ => None
end
end.

Definition inferP (t: PTerm) := inferP' [] t.

End Polymorphic.


Section Monomorphic.

Inductive MType : Type :=

| MBool : MType
| MNat : MType
| Ma : MType -> MType -> MType
. 

Inductive MTerm : Type :=
| MTermVar : nat -> MTerm 
| MLam : MType -> MTerm -> MTerm
| MApp : MTerm -> MTerm -> MTerm
| MAnd : MTerm -> MTerm -> MTerm
| MNeg : MTerm -> MTerm
| MTrue : MTerm
| MFalse : MTerm
.


Fixpoint synt_eq_typeM (A B : MType) := match (A, B) with

| (MNat, MNat) => true
| (MBool, MBool) => true
| (Ma X Y, Ma U V) => andb (synt_eq_typeM  X U) (synt_eq_typeM X V)
| (_, _) => false
end.

Fixpoint inferM' (c : list MType) (t:MTerm) : option MType :=
match t with
| MTermVar i => nth_error c i
| MLam  A u =>
match inferM' c u with
| Some B => Some (Ma A B)
| _ => None
end
| MTrue => Some MBool
| MFalse => Some MBool
| MNeg x => match inferM' c x with
        | Some MBool => Some MBool
        | _ => None
      end
| MAnd u v => match inferM' c u with 
         | Some MBool => match inferM' c v with 
                | Some MBool => Some MBool
                | _ => None
end
        | _ => None
end
| MApp u v =>
match inferM' c u, inferM' c v with
| Some (Ma A B), Some C =>
if synt_eq_typeM A C then Some B else None
| _, _ => None
end
end.

Definition inferM (t: MTerm) := inferM' [] t.


End Monomorphic.


Local Notation "A −→ B" := (Pa A B) (at level 50).

Local Notation "A --> B" :=  (Ma A B) (at level 50).


Section Casts.

(*fonctions de coertion et inductif correspondant *)

Inductive cast_result (A: Type) (n m: A) : Type :=
| Cast (k: forall P, P n -> P m)
| NoCast.


Fixpoint castNat (n1 n2 : nat) : cast_result nat n1 n2 :=
match n1, n2 with
| 0, 0 => Cast _ _ _ (fun (P : nat -> Type) (x : P 0) => x)
| 0, (S i) => NoCast _ _ _
| (S i), 0 => NoCast _ _ _
| (S i), (S j) => match castNat i j with 
        |Cast _ _ _ k' => let k P := let Pa y := P (S y) in fun x => k' Pa x in Cast _ _ _ k
        |_ => NoCast _ _ _
end
end.



Fixpoint castM (A B: MType) : cast_result MType A B :=
match A, B return cast_result MType A B with
| MBool, MBool => Cast _ _ _ (fun (P : MType -> Type) (x : P MBool) => x)
| MNat, MNat =>  Cast _ _ _ (fun (P : MType -> Type) (x : P MNat) => x)
| C --> D, E --> F =>
match castM C E, castM D F with
| Cast _ _ _ k1, Cast _ _ _ k2 =>
let k P :=
let Pb G := P (G --> D) in let Pc G := P (E --> G) in
fun x => k2 Pc (k1 Pb x)
in Cast _ _ _ k
| _, _ => NoCast _ _ _
end
| _, _ => NoCast _ _ _
end.



Fixpoint castP (A B : PType) : cast_result PType A B :=
match A as x, B as y return cast_result PType x y with
| PBool, PBool => Cast _ _ _ (fun (P : PType -> Type) (x : P PBool) => x)
| PNat, PNat =>  Cast _ _ _ (fun (P : PType -> Type) (x : P PNat) => x)
| PVar i, PVar j => match castNat i j with 
          | Cast _ _ _ k' => let k P := let Pa y := P (PVar y) in fun x => k' Pa x in Cast _ _ _ k
          | _ => NoCast _ _ _
          end
| C −→ D, E −→ F =>
match castP C E, castP D F with
| Cast _ _ _ k1, Cast _ _ _ k2 =>
let k P :=
let Pb G := P (G −→ D) in let Pc G := P (E −→ G) in
fun x => k2 Pc (k1 Pb x)
in Cast _ _ _ k
| _, _ => NoCast _ _ _
end
| _, _ => NoCast _ _ _
end.

(* l'axiome K est valable pour cast : appliqué au même objet, le cast retourne l'identité *)

Lemma castM_same : forall A, castM A A = Cast MType A A (fun _ x => x).
Proof. intros.
induction A;
unfold castM in *; simpl in * ; try (apply f_equal); try (reflexivity).
- rewrite IHA1. rewrite IHA2. reflexivity. Qed.

Lemma castNat_same : forall A, castNat A A = Cast nat A A (fun _ x => x).
Proof. intros. induction A. 
- unfold castNat. reflexivity.
- unfold castNat in *. rewrite IHA. reflexivity. Qed.

Lemma castP_same : forall A, castP A A = Cast PType A A (fun _ x => x).
Proof. intros. induction A; unfold castP in *; simpl in *; try (apply f_equal); 
try (reflexivity). 
- unfold castNat. rewrite castNat_same. reflexivity.
- rewrite IHA1. rewrite IHA2. reflexivity. Qed.

End Casts.


Section Interpretation.

(* interprétation des types par les types de Coq *)

Fixpoint interp_typeM (T: MType) :=
match T with
| MBool => bool
| MNat =>  nat
| Ma A B => (interp_typeM A) -> (interp_typeM B)
end.

Definition contextM := list MType.


(* on interprète les variables libres par unit et les variables du contexte par le n-ième élément 
de celui-ci *) 
Definition interp_contextM (g: contextM) : Type :=
forall (n: nat), match nth_error g n with
| Some A => interp_typeM A
| _ => unit
end.

(* fonctions pour réduire ou enrichir le contexte *)


Definition interp_tailM A g (f: interp_contextM (A::g)) :
interp_contextM g :=
fun n => f (S n).

Definition interp_consM A g (f: interp_contextM g)
(a: interp_typeM A) : interp_contextM (A::g) :=
fun n => match n with
| O => a
| S n => f n
end.

(*interprétation des variables en fonction de leur indice de De Brujin *)


Fixpoint interp_dbrM (g: contextM) (n:nat) {struct n} :
option {A: MType & interp_contextM g -> interp_typeM A} :=
match g,n return
option {A: MType & interp_contextM g -> interp_typeM A} with
| nil, _ => None
| A::g, O => Some (existT _ A (fun f => f O))
| A::g, S n =>
match interp_dbrM g n with
| Some (existT _ B b) =>
Some (existT _ B (fun f => b (interp_tailM _ _ f)))
| _ => None
end
end.

(* fonction d'interprétation 
des termes monomorphes *)

Fixpoint interp_auxM (g: contextM) (t: MTerm) :
option {A: MType & interp_contextM g -> interp_typeM A} :=
match t with
| MTermVar i => interp_dbrM g i
| MLam A u =>
match interp_auxM (A::g) u with
| Some (existT _ U c) => Some (existT (fun A : MType => interp_contextM g -> interp_typeM A) (A --> U) (fun f z => c (
(interp_consM _ _ f z))))
| _ => None
end
| MApp u v =>
match interp_auxM g u, interp_auxM g v with
| Some (existT _ (A --> B) b ), Some (existT _ C c ) =>
match castM C A with
| Cast _ _ _ k => Some (existT _ B (fun f => (b f) (k _ (c f))) )
| _ => None
end
| _, _ => None
end
| MTrue => Some (existT (fun A : MType => interp_contextM g -> interp_typeM A) MBool (fun f => true))
| MFalse => Some (existT (fun A : MType => interp_contextM g -> interp_typeM A) MBool (fun f => false))
| MNeg x => match interp_auxM g x with
          | Some (existT _ MBool y) => Some (existT (fun A : MType => interp_contextM g -> interp_typeM A) MBool (fun w => (match (y w) with 
                                                              | true => false 
                                                              | false => true
                                                        end)))

          | _ => None 
end
| MAnd x y => match interp_auxM g x with
          |  Some (existT _ MBool u) => match interp_auxM g y with 
                                  |  Some (existT _ MBool v) => Some (existT (fun A : MType => interp_contextM g -> interp_typeM A) MBool (fun w => (match (u w, v w) with 
                                                              | (true, true) => true
                                                              | (_, _) => false
                                                        end)))
                                  | _ => None
end
          | _ => None
end
end.


(*interprétation d'un contexte vide *)

Definition interp_nilM : interp_contextM nil := fun (n : nat) => tt.

(*fonction d'interprétation finale : 
deuxième projection de l'interprétation auxiliaire dans un contexte vide *)

Definition interpM t :=
match interp_auxM nil t as p return
match p with
| Some (existT _ A _) => (interp_typeM A)
| None => unit
end
with
| Some (existT _ A b) => (b (interp_nilM))
| None => tt
end.


(* fonctions similaires pour le langage polymorphe, on rajoute juste en paramètre une fonction qui 
donne l'interprétation des variables de type *)

Definition contextP := list PType.

Variable context_type : nat -> Type.

Fixpoint interp_typeP (f : nat -> Type ) (T : PType) :=
match T with 
| PNat =>  nat
| PBool => bool
| Pa A B => interp_typeP f A -> interp_typeP f B
| PVar i => f i
end.


Definition interp_contextP (f : nat -> Type ) (g: contextP)  := forall (n: nat),
match nth_error g n with
| Some A => interp_typeP f A
| _ => unit
end.


Definition interp_tailP A (h: nat -> Type) g (f: interp_contextP h (A::g)) :
interp_contextP h g :=
fun n => f (S n).

Definition interp_consP A (h: nat -> Type) g (f: interp_contextP h g)
(a: interp_typeP h A) : interp_contextP h (A::g) :=
fun n => match n with
| O => a
| S n => f n
end.

Fixpoint interp_dbrP (h: nat -> Type) (g: contextP) (n:nat) {struct n} :
option {A: PType & interp_contextP h g -> interp_typeP h A} :=
match g,n return
option {A: PType & interp_contextP h g -> interp_typeP h A} with
| nil, _ => None
| A::g, O => Some (existT _ A (fun f => f O))
| A::g, S n =>
match interp_dbrP h g n with
| Some (existT _ B b) =>
Some (existT _ B (fun f => b (interp_tailP _  _ _ f)))
| _ => None
end
end.

Fixpoint interp_auxP (h: nat -> Type) (g: contextP) (t: PTerm) :
option {A: PType & interp_contextP h g -> interp_typeP h A} :=
match t with
| PTermVar i => interp_dbrP h g i
| PLam A u =>
match interp_auxP  h (A::g) u with
| Some (existT _ U d) => Some (existT (fun A : PType => (interp_contextP h g) -> interp_typeP h A) (A −→ U) (fun f z => d (
(interp_consP _ _ _ f z))))
| _ => None
end
| PApp u v =>
match interp_auxP h g u, interp_auxP h g v with
| Some (existT _ (A −→ B) b ), Some (existT _ C c ) =>
match castP C A with
| Cast _ _ _ k => Some (existT _ B (fun f => (b f) (k _ (c f))) )
| _ => None
end
| _, _ => None
end
| PTrue => Some (existT (fun A : PType => interp_contextP h g -> interp_typeP h A) PBool (fun f => true))
| PFalse => Some (existT (fun A : PType => interp_contextP h g -> interp_typeP h A) PBool (fun f => false))
| PNeg x => match interp_auxP h g x with
          | Some (existT _ PBool y) => Some (existT (fun A : PType => interp_contextP h g -> interp_typeP h A) PBool (fun w => (match (y w) with 
                                                              | true => false 
                                                              | false => true
                                                        end)))
          | _ => None 
end
| PAnd x y => match interp_auxP h g x with
          |  Some (existT _ PBool u) => match interp_auxP h g y with 
                                  |  Some (existT _ PBool v) => Some (existT (fun A : PType => interp_contextP h g -> interp_typeP h A) PBool (fun w => (match (u w, v w) with 
                                                              | (true, true) => true
                                                              | (_, _) => false
                                                        end)))
                                  | _ => None
end
          | _ => None
end
end. 


Definition interp_nilP h : (interp_contextP h nil) := fun (n:nat) => tt.


Definition interpP h t :=
match interp_auxP h nil t as p return
match p with
| Some (existT _ A _) => option (interp_typeP h A)
| None => option bool 
end
with
| Some (existT _ A b) => Some (b (interp_nilP h))
| None => None
end.

End Interpretation.

Section Traduction.


(*traduction des types : monomorphisation *)

Fixpoint trad_type (x: PType) := match x with 
| PBool => MBool
| PVar i => MNat (* on envoie toutes les variables de types sur un built-in type, ici: Nat *)
| PNat => MNat 
| A −→ B => (trad_type A) --> (trad_type B)
end
.

(* traduction des termes, on ne fait qu'appliquer la traduction des types dans le cas lambda *)


Fixpoint trad_term (x: PTerm) := match x with
| PTermVar i  => MTermVar i 
| PLam A v => MLam (trad_type A) (trad_term v)
| PApp u v => MApp (trad_term u) (trad_term v)
| PAnd u v => MAnd (trad_term u) (trad_term v)
| PTrue => MTrue
| PFalse => MFalse
| PNeg x => MNeg (trad_term x)
end.


End Traduction.

(* Quelques lemmes utiles pour la preuve du théorème final sur les fonctions d'interprétation *)


Lemma interp_eq : forall B, interp_typeP (fun _ : nat => nat) B = interp_typeM (trad_type B).
Proof. intros. induction B; simpl in *; try (reflexivity).
- rewrite IHB1. rewrite IHB2. reflexivity.
Qed.

Lemma interp_dbrM_length : forall l n, (exists x, interp_dbrM l n = Some x) -> length l > n.
Proof. intros l n H. generalize dependent l.
induction n; intros l H; simpl in *.
- destruct l eqn : E. 
    + inversion H. inversion H0.
    + simpl in *. remember (length c) as i. omega.
- inversion H. destruct l eqn: E. inversion H0. destruct (interp_dbrM c n) eqn: G. 
destruct s as [A f]. inversion H0. specialize (IHn c). assert (length c > n).
apply IHn. destruct H2. exists (existT _ A f). apply G. simpl. omega. inversion H0. Qed.

Lemma interp_dbrP_length : forall g l n, (exists x, interp_dbrP g l n = Some x) -> length l > n.
Proof. intros g l n H. generalize dependent l.
induction n; intros l H; simpl in *.
- destruct l eqn : E. 
    + inversion H. inversion H0.
    + simpl in *. remember (length c) as i. omega.
- inversion H. destruct l eqn: E. inversion H0. destruct (interp_dbrP g c n) eqn: G. 
destruct s as [A f]. inversion H0. specialize (IHn c). assert (length c > n).
apply IHn. destruct H2. exists (existT _ A f). apply G. simpl. omega. inversion H0. Qed.


(* Le théorème principal encore à montrer qui relie l'interprétation d'un terme traduit à son interprétation 
dans le langage polymorphe *)

Theorem trad_is_sound : 
forall (t : PTerm) g c, map trad_type g = c -> match interp_auxM c (trad_term t) with
| Some (existT _ A a) => match interp_auxP (fun n => nat) g t with 
            | Some (existT _ B b) => match castM A (trad_type B) with
                                       | Cast _ _ _ k => forall (c':interp_contextM c) (g':interp_contextP (fun n => nat) g),
                                           k (fun T => interp_typeM T) (a c') = rew [fun A' => A'] (interp_eq B) in (b g')
                                       | NoCast _ _ _ => False
end
            | None => False
end
| None => True
end.
Admitted.








