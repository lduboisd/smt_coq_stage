Require Import Nat.
Set Keep Proof Equalities.
Require Import List.
Import ListNotations. 
Require Import Omega.
Require Import MSets.



 Fixpoint Map { A B } (f : A -> B) (l:list A) : list B :=
    match l with
      | [] => []
      | a :: t => (f a) :: (Map f t)
    end.



Section arithmetic_lemmas.

Lemma max_lemma : forall n n', Nat.max n n' >= n' /\ Nat.max n n' >= n.
Proof. intros n. induction n. intros n'. simpl. omega. 
intros n'. assert (forall n m : nat, {Nat.max n m = n} + {Nat.max n m = m}) as H'. apply Nat.max_dec.
specialize (H' n n'). destruct H'. split. induction n'. omega. Search Nat.max. rewrite <- Nat.succ_max_distr.
specialize (IHn n'). destruct IHn. omega. 
Search Nat.max. assert ((S n) <= Nat.max (S n) n') as H3. apply Max.le_max_l. omega. 
split. apply Max.le_max_r. apply Max.le_max_l. Qed.

Lemma le_zero : forall n,  0 >= n -> n=0.
Proof. intros n. omega. Qed.


Lemma le_not_succ : forall n m, ~ m >= n -> S m >= n -> S m = n.
Proof. intros n m H H1. assert (forall n m : nat, {n >= m} + {~ n >= m}) as H2.
apply ge_dec. specialize (H2 m n). destruct H2 as [H3 | H4].
- omega.
-  apply  not_ge in H. Search ">=". omega. Qed. 


Lemma match_impossible_arith : forall n, (match n with
    | 0 => false
    | S m' => n =? m'
    end = true) -> False.
Proof. intros n. induction n. easy. easy. Qed.

End arithmetic_lemmas.

Section Polymorphic_Type.
 
Inductive PType : Type :=
| PBool : PType
| PNat : PType
| PVar : nat -> PType (* type variable, prenex polymorphism *)
| Pa : PType -> PType -> PType
.


(*This type is decidable *) 


Lemma PType_decidable : forall (A B : PType), {eq A B} + {~ eq A B}.
Proof.
intros A; induction A as [ | | n0 | A1 IHA1 A2 IHA2 ]; intros B.
- destruct B. left. easy. right. easy. right. easy. right. easy.
- destruct B. right. easy. left. easy. right. easy. right. easy.
- destruct B as [ | | n | ]. right. easy. right. easy. destruct (n=? n0) eqn : E.
 left. apply Nat.eqb_eq in E. rewrite E. easy.
right. apply beq_nat_false in E. unfold "~". intros. inversion H. omega. right. easy.
- destruct B as [ | | | B1 B2]. right. easy. right. easy. right. easy.
specialize (IHA1 B1). specialize (IHA2 B2).
destruct IHA1 as [e | e']. destruct IHA2 as [g | g'].
rewrite e. rewrite g. left. easy. right. 
unfold "~" in *. intros H. inversion H. easy.
right. unfold "~" in *. intros H. inversion H. easy.
Qed.

Inductive PTerm : Type :=

| PTermVar : nat -> PTerm 
| PTrue : PTerm
| PFalse : PTerm
| PNeg : PTerm -> PTerm
| PAnd : PTerm -> PTerm -> PTerm
| PLam : PType -> PTerm -> PTerm (* De Brujin indexes *) 
| PApp : PTerm -> PTerm -> PTerm.



Fixpoint PLift (n: nat) (t: PTerm) := match t with 
| PTrue => PTrue
| PFalse => PFalse
| PNeg u => PNeg (PLift n u)
| PAnd u v => PAnd (PLift n u) (PLift n v)
| PTermVar i => if i <? n then PTermVar i else PTermVar (S i)
| PLam A u => PLam A (PLift (S n) u)
| PApp u v => PApp (PLift n u) (PLift n v)
end.

Fixpoint PSubst  (n: nat) (s t: PTerm) : PTerm :=
match t with
| PTrue => PTrue
| PFalse => PFalse
| PNeg u => PNeg (PSubst n s u)
| PAnd u v => PAnd (PSubst n s u) (PSubst n s v)
| PLam A u => PLam A (PSubst (S n) (PLift 0 s) u)
| PApp u v => PApp (PSubst n s u) (PSubst n s v)
| PTermVar i => match i?= n with
        | Lt => PTermVar i
        | Eq => s 
        | Gt => PTermVar (i-1)
end 
end.

(* Fixpoint PSubst_Type (l : list (nat*PType)) (T : PType) := match l with
| [] => T
| (n, P)::l' => match T with 
      | PBool => PBool
      | PNat => PNat 
      | Pa X Y => Pa (PSubst_Type l X) (PSubst_Type l Y)
      | PVar i => if n =? i then P else PSubst_Type l' T
end
end. *)

Fixpoint lenght_PType (T : PType) := match T with
| PBool => 1
| PNat => 1 
| Pa X Y => lenght_PType X + lenght_PType Y
| _ => 1
end.

Fixpoint PSubst_Type (l : list (nat*PType)) (T : PType) {measure ((lenght_PType T) + (length l))}  : PType  := match T with
| PBool => PBool
| PNat => PNat 
| Pa X Y => Pa (PSubst_Type l X) (PSubst_Type l Y)
| PVar i => match l with 
        | [] => PVar i 
        | (n, P)::l' => if n =? i then P else PSubst_Type l' (PVar i)
end
end


.

Fixpoint PSubst_Type_in_term (n: nat) (s: PType) (t: PTerm) := match t with 
| PTrue => PTrue
| PFalse => PFalse
| PNeg u => PNeg (PSubst_Type_in_term n s u)
| PAnd u v => PAnd (PSubst_Type_in_term n s u) (PSubst_Type_in_term n s v)
| PLam A u => PLam (PSubst_Type n s A) (PSubst_Type_in_term n s u)
| PApp u v => PApp (PSubst_Type_in_term n s u) (PSubst_Type_in_term n s v)
| PTermVar i => PTermVar i
end.

End Polymorphic_Type.

Local Notation "A −→ B" := (Pa A B) (at level 50).

Module PTermDecidableType <: Equalities.DecidableType.

  Definition t := PTerm.
  Definition eq : t -> t -> Prop := fun x y => x = y.
  Lemma eq_equiv : Equivalence eq. Proof. apply eq_equivalence. Qed.
  Lemma eq_dec : forall x y : t, {eq x y} + {~ eq x y}.
Proof. intros x. induction x; intros y.
- destruct y. destruct (n=? n0) eqn : E. left. apply Nat.eqb_eq in E. rewrite E. easy.
right. apply beq_nat_false in E. unfold "~". intros. inversion H. omega.
right; easy. right. easy. right. easy. right. easy. right. easy. right. easy. 
- destruct y. right. easy. left. easy. right. easy. right. easy. right. easy.
right. easy. right. easy. 
- destruct y. right. easy. right. easy. left. easy. right. easy. right. easy.
right. easy. right. easy.
- destruct y. right. easy. right. easy. right. easy. specialize (IHx y). destruct IHx as [e | e'].
left. rewrite e. easy. right. unfold "~" in *. intros H. inversion H. easy.
right. easy. right. easy. right. easy.
- destruct y. right. easy. right. easy. right. easy. right. easy. specialize (IHx1 y1).
specialize (IHx2 y2). destruct IHx1 as [e | e']. destruct IHx2 as [g | g']. left. rewrite e. rewrite g. easy.
right. unfold "~" in *. intros H. inversion H. easy. right. unfold "~" in *. intros H. inversion H. easy.
right. easy. right. easy. 
- destruct y. right. easy. right. easy. right. easy. right. easy.
right. easy. specialize (IHx y). destruct IHx as [e | e']. assert ({p= p0} + {~ p= p0}) as H.
apply PType_decidable. destruct H as [g | g']. left. rewrite e. rewrite g. easy.
right. unfold "~" in *. intros H. inversion H. easy. right.
unfold "~" in *. intros H. inversion H. easy. right. easy.
-  destruct y. right. easy. right. easy. right. easy. right. easy. 
right. easy. right. easy. specialize (IHx1 y1).
specialize (IHx2 y2). destruct IHx1 as [e | e']. destruct IHx2 as [g | g']. left. rewrite e. rewrite g. easy.
right. unfold "~" in *. intros H. inversion H. easy. right. unfold "~" in *. intros H. inversion H. easy.
Qed.

End PTermDecidableType.

Module STT (Se:WSetsOn PTermDecidableType).
Definition PTerm_set := Se.t.


Section Polymorphic_Rules.

Definition contextP := PTerm_set.

Definition context_typeP := list PType.

Fixpoint syn_eq_typeP (A B : PType) := match (A, B) with
|(PBool, PBool) => true
|(PNat, PNat) => true
|(PVar i, PVar j) => i =? j
|(U −→ V, W −→ X) => andb (syn_eq_typeP U W) (syn_eq_typeP V X)
| (_, _) => false
end. 

Fixpoint infer_typeP (c: context_typeP) (t: PTerm) : option PType := match t with
| PTermVar i => nth_error c i
| PTrue => Some PBool
| PFalse => Some PBool
| PNeg x => match infer_typeP c x with 
              | Some PBool => Some PBool
              | _ => None
              end
| PAnd x y => match infer_typeP c x with 
              | Some PBool =>  match infer_typeP c y with 
                          | Some PBool => Some PBool
                          | _ => None
                           end
              | _ => None
              end
| PLam A v => match infer_typeP (A::c) v with 
          |Some B => Some (A −→ B)
          | _ => None 
      end
| PApp u v => match infer_typeP c u, infer_typeP c v with
| Some (A −→ B), Some C =>
if syn_eq_typeP A C then Some B else None
| _, _ => None
end
end.

Definition context_okP (X : contextP) := (forall (x : PTerm), Se.In x X -> (infer_typeP [] x = Some PBool)).

Inductive ConvP : PTerm -> PTerm -> Prop :=
| ConvPRefl : forall (u: PTerm), ConvP u u
| ConvPSym : forall (u v : PTerm), ConvP u v -> ConvP v u
| ConvPTrans : forall (u v w : PTerm), ConvP u v -> ConvP v w -> ConvP u w
| ConvPNeg1 : ConvP (PNeg (PTrue)) PFalse
| ConvPNeg2 : ConvP (PNeg PFalse) PTrue
| ConvPAnd1 : forall (x : PTerm), ConvP (PAnd x PTrue) x
| ConvPAnd2 : forall (y : PTerm), ConvP (PAnd PTrue y) y
| ConvPAnd3 : forall (x: PTerm), ConvP (PAnd x PFalse) PFalse
| ConvPAnd4 : forall (y: PTerm), ConvP (PAnd PFalse y) PFalse
| ConvPCongLam : forall (u v : PTerm), ConvP u v -> (forall (A : PType), ConvP (PLam A u) (PLam A v))
| ConvPCongApp : forall (u u' v v': PTerm), ConvP u u' -> ConvP v v' -> ConvP (PApp u v) (PApp u' v')
| ConvPBeta    : forall  (A: PType) (t u: PTerm), ConvP (PApp (PLam A t) u) (PSubst 0 u t)
.


Inductive validP : contextP -> PTerm -> Prop :=
| SemPTrue : forall (c : contextP), context_okP c -> validP c PTrue
| SemPConv : forall c (t u : PTerm), context_okP c -> ConvP t u -> validP c t -> validP c u 
| SemPAnd : forall  c (t u : PTerm), context_okP c -> validP c t -> validP c u -> validP c (PAnd t u)
| SemPAssume: forall c (t u : PTerm), context_okP c -> Se.In t c -> validP c t
| SemPSubst : forall c (t: PTerm) (i: nat) (s: PType), context_okP c -> validP c t -> validP c (PSubst_Type_in_term i s t).


End Polymorphic_Rules.

Section Monomorphic_Type.

Inductive MType : Type :=

| MBool : MType
| MNat : MType
| Ma : MType -> MType -> MType
. 

Inductive MTerm : Type :=
| MTrue : MTerm
| MFalse : MTerm
| MNeg : MTerm -> MTerm
| MAnd : MTerm -> MTerm -> MTerm
| MVar : nat -> MTerm 
| MLam : MType -> MTerm -> MTerm
| MApp : MTerm -> MTerm -> MTerm.



Lemma MType_decidable : forall (A B : MType), {eq A B} + {~ eq A B}.
Proof.
intros A; induction A as [ | | A1 IHA1 A2 IHA2 ]; intros B.
- destruct B. left. easy. right. easy. right. easy.
- destruct B. right. easy. left. easy. right. easy.
- destruct B as [ | | B1 B2]. right. easy. right. easy.
specialize (IHA1 B1). specialize (IHA2 B2).
destruct IHA1 as [e | e']. destruct IHA2 as [g | g'].
rewrite e. rewrite g. left. easy. right. 
unfold "~" in *. intros H. inversion H. easy.
right. unfold "~" in *. intros H. inversion H. easy.
Qed.

Fixpoint MLift (n: nat) (t: MTerm) := match t with 
| MTrue => MTrue
| MFalse => MFalse
| MNeg u => MNeg (MLift n u)
| MAnd u v => MAnd (MLift n u) (MLift n v)
| MVar i => if i <? n then MVar i else MVar (S i)
| MLam A u => MLam A (MLift (S n) u)
| MApp u v => MApp (MLift n u) (MLift n v)
end.





Fixpoint MSubst (n: nat) (s t: MTerm) : MTerm :=
match t with
| MTrue => MTrue
| MFalse => MFalse
| MNeg u => MNeg (MSubst n s u)
| MAnd u v => MAnd (MSubst n s u) (MSubst n s v)
| MLam A u => MLam A (MSubst (S n) (MLift 0 s) u)
| MApp u v => MApp (MSubst n s u) (MSubst n s v)
| MVar i => match i?= n with
        | Lt => MVar i
        | Eq => s 
        | Gt => MVar (i-1)
end 
end.


End Monomorphic_Type.


Local Notation "A ---> B" :=  (Ma A B) (at level 50).



Module MTermDecidableType <: Equalities.DecidableType.

  Definition t := MTerm.
  Definition eq : t -> t -> Prop := fun x y => x = y.
  Lemma eq_equiv : Equivalence eq. Proof. apply eq_equivalence. Qed.
  Lemma eq_dec : forall x y : t, {eq x y} + {~ eq x y}. Proof.
intros x. induction x as [ | | x IHx | x1 IHx1 x2 IHx2 | n | m x IHx | x1 IHx1 x2 IHx2  ] ; intros y.

- destruct y. left. easy. right. easy. right. easy. right. easy.
right. easy. right. easy. right. easy. 
- destruct y. right. easy.  left. easy. right. easy. right. easy.
right. easy. right. easy. right. easy.
- destruct y. right. easy. right. easy.  specialize (IHx y). destruct IHx as [e | e'].
left. rewrite e. easy. right. unfold "~" in *. intros H. inversion H. easy.
right. easy. right. easy. right. easy. right. easy.
- destruct y. right. easy. right. easy. right. easy.  specialize (IHx1 y1).
specialize (IHx2 y2). destruct IHx1 as [e | e']. destruct IHx2 as [g | g']. left. rewrite e. rewrite g. easy.
right. unfold "~" in *. intros H. inversion H. easy. right. unfold "~" in *. intros H. inversion H. easy.
right. easy. right. easy. right. easy.
- destruct y. right. easy. right. easy.  right. easy.   right. easy.  destruct (n=? n0) eqn : E. left. apply Nat.eqb_eq in E. rewrite E. easy.
right. apply beq_nat_false in E. unfold "~". intros. inversion H. omega. 
right. easy. right. easy. 
- destruct y. right. easy. right. easy. right. easy. right. easy. right. easy. 
 specialize (IHx y). destruct IHx as [e | e']. assert ({m= m0} + {~ m= m0}) as H.
apply MType_decidable. destruct H as [g | g']. left. rewrite e. rewrite g. easy.
right.  unfold "~" in *. intros H. inversion H. easy. right. unfold "~" in *. intros H. inversion H. easy.
right. easy.
-  destruct y. right. easy. right. easy. right. easy. right. easy. 
right. easy. right. easy. specialize (IHx1 y1).
specialize (IHx2 y2). destruct IHx1 as [e | e']. destruct IHx2 as [g | g']. left. rewrite e. rewrite g. easy.
right. unfold "~" in *. intros H. inversion H. easy. right. unfold "~" in *. intros H. inversion H. easy.
Qed.

End MTermDecidableType.


Module STT' (Se':WSetsOn MTermDecidableType).


Definition MTerm_set := Se'.t.

Definition set : PTerm_set :=
    Se.union (Se.singleton PTrue) (Se.union (Se.singleton PFalse) (Se.singleton PTrue)).
Definition set' : MTerm_set :=
    Se'.union (Se'.singleton MTrue) (Se'.union (Se'.singleton MFalse) (Se'.singleton MTrue)).

Module Sep := WPropertiesOn PTermDecidableType Se.


Definition map f (s:Se.t) : Se'.t :=
  Se.fold (fun e acc => Se'.add (f e) acc) s Se'.empty.


Lemma map_spec1 f s x : Se.In x s -> Se'.In (f x) (map f s).
Proof.
  unfold map.
  apply (@Sep.fold_rec _ (fun si so => Se.In x si -> Se'.In (f x) so)).
  - intros s' Hs' HIn. now elim (Hs' _ HIn).
  - intros y s1 s2 s3 H1 H2 H3 H4 H5.
    rewrite Se'.add_spec.
    unfold Sep.Add in H3. rewrite H3 in H5. destruct H5 as [H5|H5].
    + left. now rewrite H5.
    + right. now apply H4.
Qed.

Lemma map_spec2 f s y : Se'.In y (map f s) -> exists x, Se.In x s /\ y =
f x.
Proof.
  unfold map.
  apply (@Sep.fold_rec _ (fun si so => Se'.In y so ->
                                       exists x : Se.elt, Se.In x si /\
y = f x)).
  - intros s1 _ H. now elim (Se'.empty_spec H).
  - intros x s1 s2 s3 H1 H2 H3 H4 H5.
    unfold Sep.Add in H3.
    rewrite Se'.add_spec in H5. destruct H5 as [H5|H5].
    + subst y. exists x. split; auto.
      rewrite H3. now left.
    + destruct (H4 H5) as [z [Hz1 Hz2]].
      exists z. split; auto.
      rewrite H3. now right.
Qed.


Lemma map_union f s1 s2 :
  Se'.Equal (map f (Se.union s1 s2)) (Se'.union (map f s1) (map f s2)).
Proof.
  unfold Se'.Equal. intro a; split; intro H.
  - apply map_spec2 in H. destruct H as [x [Hx1 Hx2]]. subst a.
    rewrite Se.union_spec in Hx1.
    rewrite Se'.union_spec. destruct Hx1 as [Hx1|Hx1].
    + left. now apply map_spec1.
    + right. now apply map_spec1.
  - rewrite Se'.union_spec in H. destruct H as [H|H].
    + apply map_spec2 in H. destruct H as [x [H ->]].
      apply map_spec1. rewrite Se.union_spec. now left.
    + apply map_spec2 in H. destruct H as [x [H ->]].
      apply map_spec1. rewrite Se.union_spec. now right.
Qed.


Section Monomorphic_Rules.

Inductive ConvM : MTerm -> MTerm -> Prop :=
| ConvMRefl : forall (u: MTerm), ConvM u u
| ConvMSym : forall (u v : MTerm), ConvM u v -> ConvM v u
| ConvMTrans : forall (u v w : MTerm), ConvM u v -> ConvM v w -> ConvM u w
| ConvMNeg1 : ConvM (MNeg (MTrue)) MFalse
| ConvMNeg2 : ConvM (MNeg MFalse) MTrue
| ConvMAnd1 : forall (x : MTerm), ConvM (MAnd x MTrue) x
| ConvMAnd2 : forall (y : MTerm), ConvM (MAnd MTrue y) y
| ConvMAnd3 : forall (x: MTerm), ConvM (MAnd x MFalse) MFalse
| ConvMAnd4 : forall (y: MTerm), ConvM (MAnd MFalse y) MFalse
| ConvMCongMam : forall (u v : MTerm), ConvM u v -> (forall (A : MType), ConvM (MLam A u) (MLam A v))
| ConvMCongApp : forall (u u' v v': MTerm), ConvM u u' -> ConvM v v' -> ConvM (MApp u v) (MApp u' v')
| ConvMBeta    : forall  (A: MType) (t u: MTerm), ConvM (MApp (MLam A t) u) (MSubst 0 u t)
.

Definition contextM := MTerm_set.

Definition context_typeM := list MType.

Fixpoint syn_eq_typeM (A B : MType) := match (A, B) with
|(MBool, MBool) => true
|(MNat, MNat) => true
|(U ---> V, W ---> X) => andb (syn_eq_typeM U W) (syn_eq_typeM V X)
| (_, _) => false
end. 

Fixpoint infer_typeM (c: context_typeM) (t: MTerm) : option MType := match t with
| MVar i => nth_error c i
| MTrue => Some MBool
| MFalse => Some MBool
| MNeg x => match infer_typeM c x with 
              | Some MBool => Some MBool
              | _ => None
              end
| MAnd x y => match infer_typeM c x with 
              | Some MBool =>  match infer_typeM c y with 
                          | Some MBool => Some MBool
                          | _ => None
                           end
              | _ => None
              end
| MLam A v => match infer_typeM (A::c) v with 
          |Some B => Some (A ---> B)
          | _ => None 
      end
| MApp u v => match infer_typeM c u, infer_typeM c v with
| Some (A ---> B), Some C =>
if syn_eq_typeM A C then Some B else None
| _, _ => None
end
end.

Definition context_okM (X : contextM) := (forall (x : MTerm), Se'.In x X -> (infer_typeM [] x = Some MBool)).



Inductive validM : contextM -> MTerm -> Prop := 
| SemMTrue : forall c, context_okM c -> validM c MTrue
| SemMConv : forall c (t u : MTerm), context_okM c -> ConvM t u -> validM c t -> validM c u
| SemMAssume: forall c (t u : MTerm), context_okM c -> Se'.In t c -> validM c t
| SemMAnd : forall c (t u : MTerm), context_okM c  -> validM c t -> validM c u -> validM c (MAnd t u).


End Monomorphic_Rules.

Section traduction.

Fixpoint trad_type (x: PType) := match x with 
| PBool => MBool
| PVar i => MNat (* type variables instantiated by Nat *)
| PNat => MNat 
| A −→ B => (trad_type A) ---> (trad_type B)
end
.

Fixpoint trad_term (x: PTerm) := match x with
| PTermVar i  => MVar i 
| PTrue => MTrue
| PFalse => MFalse
| PNeg x => MNeg (trad_term x)
| PAnd x y => MAnd (trad_term x) (trad_term y)
| PLam A v => MLam (trad_type A) (trad_term v)
| PApp u v => MApp (trad_term u) (trad_term v)
end
.

Fixpoint inj_Type (A : MType) := match A with
| MNat => PNat
| MBool => PBool
| Ma X Y => Pa (inj_Type X) (inj_Type Y)
end.


Fixpoint inj (t: MTerm) := match t with 
| MApp u v => PApp (inj u) (inj v)
| MLam A x => PLam (inj_Type A) (inj x)
| MTrue => PTrue
| MFalse => PFalse
| MNeg x => PNeg (inj x)
| MAnd x y => PAnd (inj x) (inj y)
| MVar i => PTermVar i
end.

Definition trad_set (s: contextP) := map trad_term s.


Lemma exists_tradType : forall (M: MType), exists (P: PType), trad_type P = M.
Proof. intros. induction M. 
- exists PBool. simpl. reflexivity.
- exists PNat. simpl. reflexivity.
- destruct IHM1. destruct IHM2. exists (Pa x x0). simpl. rewrite H. rewrite H0.
reflexivity.
Qed. 

 

Lemma exists_tradTerm : forall (l: MTerm), exists (p : PTerm), trad_term p = l.
Proof. intros. induction l.
- exists PTrue. simpl. reflexivity.
- exists PFalse. simpl. reflexivity.
- destruct IHl. exists (PNeg x). simpl. apply f_equal. apply H.
- destruct IHl1. destruct IHl2. exists (PAnd x x0). simpl. 
rewrite H. rewrite H0. reflexivity.
- exists (PTermVar n). simpl. reflexivity.
- destruct IHl. assert (exists (P: PType), trad_type P = m). apply exists_tradType.
destruct H0. exists (PLam x0 x). simpl. rewrite H. rewrite H0. reflexivity.
- destruct IHl1. destruct IHl2. exists (PApp x x0). simpl. rewrite H. rewrite H0. reflexivity.
Qed. 


Lemma trad_inj_type : forall A, trad_type (inj_Type A) = A.
Proof. intros A; induction A; simpl in *; try reflexivity.
- rewrite IHA1. rewrite IHA2. reflexivity. Qed. 

 Lemma trad_inj : forall t, trad_term (inj t) = t.
Proof. intros t; induction t; simpl; try reflexivity.
- rewrite IHt; reflexivity.
- rewrite IHt1; rewrite IHt2; reflexivity.
- rewrite IHt. rewrite trad_inj_type. reflexivity.
-  rewrite IHt1; rewrite IHt2; reflexivity.
Qed.


Lemma Inj_Lift : forall u n, PLift n (inj u) = inj (MLift n u).
Proof. intros u. induction u; simpl; try reflexivity; intros i.
- rewrite IHu. reflexivity.
- rewrite IHu1. rewrite IHu2. reflexivity.
- destruct (n <? i ); simpl; reflexivity.
- rewrite IHu. reflexivity.
- rewrite IHu1. rewrite IHu2. reflexivity.
Qed.


Lemma Inj_Subst : forall u t n, inj (MSubst n u t) = PSubst n (inj u) (inj t).
Proof. intros u t. generalize dependent u. induction t; intros u i; simpl; try reflexivity.
- rewrite (IHt u). reflexivity.
- rewrite IHt1. rewrite IHt2. reflexivity.
- destruct (n ?= i); simpl; reflexivity.
- rewrite (IHt (MLift 0 ( u)) (S i)) . rewrite Inj_Lift. reflexivity.
- rewrite IHt1. rewrite IHt2. reflexivity.
Qed.
    

Lemma Conv_LP : forall u v, ConvM u v -> ConvP (inj u) (inj v).
Proof. intros u v H. induction H; simpl in *.
- econstructor.
- apply ConvPSym. easy.
- eapply ConvPTrans. eauto. easy.
- apply ConvPNeg1.
- apply ConvPNeg2.
- apply ConvPAnd1.
- apply ConvPAnd2.
- apply ConvPAnd3.
- apply ConvPAnd4.
- apply ConvPCongLam. easy.
- apply ConvPCongApp. easy. easy.
- rewrite Inj_Subst. apply ConvPBeta.
Qed.

Lemma trad_type_inv : forall A, trad_type (inj_Type A) = A.
Proof. intros A. induction A; simpl; try reflexivity.
- rewrite IHA1; rewrite IHA2; reflexivity.
Qed.

Lemma trad_inv : forall x, trad_term (inj x) = x.
Proof.
intros ; induction x; simpl ; try reflexivity.
- rewrite IHx. reflexivity.
- rewrite IHx1. rewrite IHx2. reflexivity.
- rewrite trad_type_inv. rewrite IHx. easy.
- rewrite IHx1. rewrite IHx2. easy.
Qed. 


End traduction.

(*Definition of compo : iterated application of a function depending on an index *)

Section commu_lemmas.

Fixpoint compo_n {A } (n: nat) (f : nat -> A -> A) (x : A) := match n with 
| 0 => x
| S n' => compo_n n' f (f n' x)
end.


Lemma commu_prop_var : forall n n0, compo_n n (fun i u => PSubst_Type_in_term i PNat u) (PTermVar n0) = PTermVar n0.
Proof. intros n n0.
induction n.
- simpl. reflexivity.
- simpl. apply IHn. Qed.

Lemma commu_prop_true : forall n, compo_n n (fun i u => PSubst_Type_in_term i PNat u) PTrue = PTrue.
Proof. intros n.
induction n.
- simpl. intuition.
- simpl. apply IHn. Qed.

Lemma commu_prop_false : forall n, compo_n n (fun i u => PSubst_Type_in_term i PNat u) PFalse = PFalse.
Proof. intros n.
induction n.
- simpl. intuition.
- simpl. apply IHn. Qed.


Lemma prop_neg_subst : forall n x, compo_n n (fun i u => PSubst_Type_in_term i PNat u) (PNeg x)= 
PNeg (compo_n n (fun i u => PSubst_Type_in_term i PNat u) x).
Proof. intros n. induction n.
- simpl. intuition.
- simpl. intros x. specialize (IHn (PSubst_Type_in_term n PNat x)). easy.
Qed.

Lemma commu_prop_neg : forall n x, PNeg (compo_n n (fun i u => PSubst_Type_in_term i PNat u) (PSubst_Type_in_term n PNat x) ) =
compo_n n (fun i u => PSubst_Type_in_term i PNat u) (PNeg (PSubst_Type_in_term n PNat x)).
Proof. intros n. induction n.
- simpl. intuition.
- simpl. intros x. specialize (IHn (PSubst_Type_in_term (S n) PNat x)). easy.
Qed.

Lemma prop_and_subst : forall n x1 x2, (PSubst_Type_in_term n PNat (PAnd x1 x2)) = PAnd (PSubst_Type_in_term n PNat x1) 
(PSubst_Type_in_term n PNat x2). Proof. intros n. induction n.
- simpl. intuition.
- simpl. intros x1 x2. specialize (IHn (PSubst_Type_in_term n PNat x1) (PSubst_Type_in_term n PNat x2)).  easy.
Qed.

Lemma commu_prop_and : forall n x1 x2, compo_n n (fun i u => PSubst_Type_in_term i PNat u) (PAnd x1 x2) =
PAnd (compo_n n (fun i u => PSubst_Type_in_term i PNat u) x1) (compo_n n (fun i u => PSubst_Type_in_term i PNat u) x2).
Proof. intros n. induction n.
- simpl. intuition.
- simpl in *. intros x1 x2. specialize (IHn (PSubst_Type_in_term n PNat x1) (PSubst_Type_in_term n PNat x2)). 
rewrite IHn. easy. Qed. 

Lemma prop_app_subst : forall n x1 x2, (PSubst_Type_in_term n PNat (PApp x1 x2)) = PApp (PSubst_Type_in_term n PNat x1) 
(PSubst_Type_in_term n PNat x2). Proof. reflexivity. Qed.


Lemma commu_prop_app : forall n x1 x2, compo_n n (fun i u => PSubst_Type_in_term i PNat u) (PApp x1 x2)=
PApp (compo_n n (fun i u => PSubst_Type_in_term i PNat u) x1) (compo_n n (fun i u => PSubst_Type_in_term i PNat u) x2).
Proof. intros n. induction n.
- simpl. intuition.
- simpl in *. intros x1 x2. specialize (IHn (PSubst_Type_in_term n PNat x1) (PSubst_Type_in_term n PNat x2)). 
rewrite IHn. easy. Qed. 

Lemma compo_neg : forall n A x, compo_n n (fun i u => PSubst_Type_in_term i A u)

 (PNeg x) = PNeg (compo_n n (fun i u => PSubst_Type_in_term i A u) x).
Proof. intros n. induction n; intros A x.
- simpl. reflexivity.
- simpl. apply IHn.
Qed.

Lemma compo_and : forall n A x1 x2, compo_n n (fun i u => PSubst_Type_in_term i A u) (PAnd x1 x2) = 
PAnd (compo_n n (fun i u => PSubst_Type_in_term i A u) x1)
(compo_n n (fun i u => PSubst_Type_in_term i A u) x2).
Proof. intros n. induction n; intros A x1 x2.
- simpl. reflexivity.
- simpl. apply IHn.
Qed.



Lemma compo_app : forall n A x1 x2, compo_n n (fun i u => PSubst_Type_in_term i A u) (PApp x1 x2) = 
PApp (compo_n n (fun i u => PSubst_Type_in_term i A u) x1)
(compo_n n (fun i u => PSubst_Type_in_term i A u) x2).
Proof. intros n. induction n; intros A x1 x2.
- simpl. reflexivity.
- simpl. apply IHn.
Qed.


Lemma commu_prop_lam_type : forall n A1 A2 B, compo_n n (fun i C => PSubst_Type i B C) (A1 −→ A2) = 
Pa (compo_n n (fun i C => PSubst_Type i B C) A1) (compo_n n (fun i C => PSubst_Type i B C) A2).
Proof. intros n; induction n; intros.
- simpl. reflexivity.
- simpl. apply IHn. Qed.

Lemma commu_prop_lam : forall n A B x', compo_n n (fun i u => PSubst_Type_in_term i B u) (PLam A x') =
PLam (compo_n n (fun i C => PSubst_Type i B C) A) (compo_n n (fun i u => PSubst_Type_in_term i B u) x').
Proof. intros n; induction n; intros A B x'.
- simpl. reflexivity.
- simpl. apply IHn. Qed.


Lemma commu_prop_PNat : forall n, compo_n n (fun i C => PSubst_Type i PNat C) PNat = PNat.
Proof. intros n.
induction n.
- simpl. intuition.
- simpl. apply IHn. Qed.

Lemma commu_prop_PBool : forall n, compo_n n (fun i C => PSubst_Type i PNat C) PBool = PBool.
Proof. intros n.
induction n.
- simpl. intuition.
- simpl. apply IHn. Qed.

Lemma max_var : forall n m, m >= n -> compo_n n (fun (i : nat) (C : PType) => PSubst_Type i PNat C) (PVar m) = PVar m.
Proof. intros n; induction n; intros m H.
- simpl. reflexivity.
- simpl.
 assert (m >= n /\ m <> n) as H0. clear -H. omega.
destruct H0 as [H1 H2]. apply IHn in H1. destruct (m =? n) eqn: E ; auto. 
elim H2. Search "=?". apply beq_nat_true. easy. Qed.

Lemma min_var : forall n m, m < n -> compo_n n (fun (i : nat) (C : PType) => PSubst_Type i PNat C) (PVar m) = PNat.
Proof. intros n; induction n; intros m H.
- simpl. omega.
- simpl.
 assert (m = n \/ m < n) as H0. clear -H. omega.
destruct H0 as [H1 | H2]. subst. rewrite Nat.eqb_refl. apply commu_prop_PNat. replace (m =? n) with false. 
apply IHn. easy. symmetry. rewrite Nat.eqb_neq. omega. Qed.


Lemma commu_compo_PSubstType : forall n p,
  (PSubst_Type n PNat
     (compo_n n (fun (i : nat) (C : PType) => PSubst_Type i PNat C) p)) =

  (compo_n n (fun (i : nat) (C : PType) => PSubst_Type i PNat C)
     (PSubst_Type n PNat p)).
Proof. intros n p; induction p.
- simpl. rewrite commu_prop_PBool. easy.
- simpl. rewrite commu_prop_PNat. easy.
- simpl. destruct (Nat.leb n n0) eqn: E.
        * destruct (n0 =? n) eqn: F.
      + rewrite max_var. simpl. rewrite F. rewrite commu_prop_PNat. easy. 
apply beq_nat_true in F. omega.
      + rewrite max_var. simpl. rewrite F. reflexivity. Search Nat.leb. apply leb_complete in E. omega.
        * replace (n0 =? n) with false. rewrite min_var. simpl. easy. apply leb_complete_conv. easy.
symmetry. rewrite Nat.eqb_neq. apply leb_complete_conv in E. omega.
- simpl. rewrite commu_prop_lam_type. simpl. rewrite IHp1. rewrite IHp2. 
rewrite commu_prop_lam_type. easy. Qed.

Lemma commu_compo_PSubstTerm : forall n x, PSubst_Type_in_term n PNat (compo_n n (fun i x => PSubst_Type_in_term i PNat x) x) = 
(compo_n n (fun i x => PSubst_Type_in_term i PNat x) (PSubst_Type_in_term n PNat x)).
Proof. intros n. induction x.
    * simpl. rewrite commu_prop_var. simpl. easy.
    * simpl. rewrite commu_prop_true. simpl. easy.
    * simpl. rewrite commu_prop_false. simpl. easy.
    * rewrite prop_neg_subst. simpl. rewrite IHx. rewrite commu_prop_neg. easy.
    * rewrite prop_and_subst. simpl. rewrite commu_prop_and. simpl.  rewrite IHx1. rewrite IHx2. 
rewrite commu_prop_and. easy.
     * simpl. rewrite commu_prop_lam.  simpl. rewrite IHx. rewrite commu_prop_lam. rewrite commu_compo_PSubstType. easy.  
     * rewrite prop_app_subst. simpl. rewrite commu_prop_app. simpl.  rewrite IHx1. rewrite IHx2. 
rewrite commu_prop_app. easy. Qed.


End commu_lemmas.


Lemma conv_Valid : forall t u c, context_okP c -> ConvM t u -> ((validP c (inj t) -> validP c (inj u)) /\ 
(validP c (inj u) -> validP c (inj t))).
Proof. intros t u c G H. induction H.
- easy.
- split.
     + intros H1. destruct IHConvM as [H2 H3]. apply H3 in H1. apply H1.
     + intros H1. destruct IHConvM as [H2 H3]. apply H2 in H1. apply H1.
- split. 
    + intros H1. destruct IHConvM1 as [H2 H3]. destruct IHConvM2 as [H4 H5].
apply H2 in H1. apply H4 in H1. easy.
    + intros H1. destruct IHConvM1 as [H2 H3]. destruct IHConvM2 as [H4 H5].
apply H5 in H1. apply H3 in H1. easy.
- simpl; split.
    + intros H. assert (ConvP (PNeg PTrue) PFalse). apply ConvPNeg1. eapply SemPConv. apply G.
apply H0. apply H.
    + intros H. assert (ConvP (PNeg PTrue) PFalse). apply ConvPNeg1. eapply SemPConv. apply G. apply ConvPSym.
apply H0. apply H.
- simpl; split.
    + intros H. assert (ConvP (PNeg PFalse) PTrue). apply ConvPNeg2. eapply SemPConv. apply G.
apply H0. apply H.
    + intros H. assert (ConvP (PNeg PFalse) PTrue). apply ConvPNeg2. eapply SemPConv. apply G. apply ConvPSym.
apply H0. apply H.
- simpl; split; intros H.
    + assert (ConvP (PAnd (inj x) PTrue) (inj x)) as H0. apply ConvPAnd1. eapply SemPConv. apply G. apply H0.
apply H.
    + assert (ConvP (PAnd (inj x) PTrue) (inj x)) as H0. apply ConvPAnd1. eapply SemPConv. apply G. apply ConvPSym. apply H0.
apply H.
- simpl; split.
    + assert (ConvP (PAnd PTrue (inj y)) (inj y)) as H0. apply ConvPAnd2. eapply SemPConv. apply G. apply H0.
    + assert (ConvP (PAnd PTrue (inj y)) (inj y)) as H0. apply ConvPAnd2. eapply SemPConv. apply G. apply ConvPSym. apply H0.
- simpl; split; intros H.
    + assert (ConvP (PAnd (inj x) PFalse) PFalse) as H0. apply ConvPAnd3. eapply SemPConv. apply G. apply H0.
apply H.
    + assert (ConvP (PAnd (inj x) PFalse) PFalse) as H0. apply ConvPAnd3. eapply SemPConv. apply G. apply ConvPSym. apply H0.
apply H.
- simpl; split.
    + assert (ConvP (PAnd PFalse (inj y)) PFalse) as H0. apply ConvPAnd4. eapply SemPConv. apply G. apply H0.
    + assert (ConvP (PAnd PFalse (inj y)) PFalse) as H0. apply ConvPAnd4. eapply SemPConv. apply G. apply ConvPSym. apply H0.
- destruct IHConvM as [H1 H2]. split.
    + intros H3. simpl in *. apply ConvMCongMam with _ _ A in H. eapply SemPConv. apply G. apply Conv_LP in H. simpl in H. 
apply H. apply H3.
    + intros H3. simpl in *. apply ConvMCongMam with _ _ A in H. eapply SemPConv. apply G. apply Conv_LP in H. 
apply ConvPSym. apply H. apply H3.
- destruct IHConvM1 as [H2 H3]. destruct IHConvM2 as [H4 H5]. simpl in *. split.
    + intros H'. apply Conv_LP in H. apply Conv_LP in H0. 
apply ConvPCongApp with (inj u) (inj u') (inj v) (inj v') in H.
eapply SemPConv. apply G. apply H. apply H'. easy.
    + intros H'. apply Conv_LP in H. apply Conv_LP in H0. 
apply ConvPCongApp with (inj u) (inj u') (inj v) (inj v') in H.
eapply SemPConv. apply G. eapply ConvPSym. apply H. apply H'. easy.
- split.
     + intros H. assert (ConvM (MApp (MLam A t) u) (MSubst 0 u t)) as H1. apply ConvMBeta.
apply Conv_LP in H1. rewrite Inj_Subst in *. simpl in *. eapply SemPConv. apply G. apply H1. apply H.
    + intros H. assert (ConvM (MApp (MLam A t) u) (MSubst 0 u t)) as H1. apply ConvMBeta.
apply Conv_LP in H1. rewrite Inj_Subst in *. simpl in *. eapply SemPConv. apply G. apply ConvPSym. apply H1. apply H. 
Qed.



Lemma syn_eq_type_MP : forall A B, syn_eq_typeM A B = true -> syn_eq_typeP (inj_Type A) (inj_Type B) = true.
Proof. intros A; induction A; intros B H.
- unfold syn_eq_typeM in H. destruct B; try discriminate. reflexivity.
- unfold syn_eq_typeM in H. destruct B; try discriminate. reflexivity.
- simpl in *. destruct B; try discriminate. unfold "&&" in H. destruct (syn_eq_typeM A1 B1) eqn : E.
apply IHA1 in E. apply IHA2 in H. simpl in *. unfold "&&". rewrite E. rewrite H. easy.
destruct H. apply IHA1 in E. simpl. rewrite E. unfold "&&". reflexivity.
Qed.

Lemma infer_type_trad : forall x c A, infer_typeM c x = Some A -> infer_typeP (Map inj_Type c) (inj x) = Some (inj_Type A).
Proof. intros x ; induction x; intros c A H.
- inversion H; subst. simpl in *. reflexivity.
- inversion H; subst. simpl in *. reflexivity.
- inversion H; subst. simpl in *. destruct (infer_typeM c x) as [a | ] eqn : G. destruct a; inversion H. apply IHx in G. 
rewrite G. simpl. reflexivity. inversion H. 
- inversion H; subst. simpl in *. destruct (infer_typeM c x1) as [a | ] eqn : G. destruct a; inversion H.
destruct (infer_typeM c x2) as [b | ] eqn : J. destruct b; inversion H2.
 apply IHx1 in G. apply IHx2 in J. 
rewrite G. simpl. rewrite J. simpl. reflexivity. inversion H. inversion H. 
- generalize dependent c. induction n. intros c H. 
      + destruct c eqn : F.  apply nth_error_In in H. apply in_nil in H. destruct H.
       simpl in *. inversion H. reflexivity.
      + destruct c eqn : F. intros H1.  apply nth_error_In in H1. apply in_nil in H1. destruct H1.
simpl in *. intros H1. apply IHn. apply H1.
- inversion H as [H1]; subst. simpl in *. destruct (infer_typeM (m :: c) x) eqn : F. apply IHx in F. simpl in *.
rewrite F. inversion H. simpl. reflexivity. inversion H.
- simpl in *. destruct (infer_typeM c x1) eqn : E. apply IHx1 in E. rewrite E. destruct m eqn: G; try discriminate.
simpl in *. destruct (infer_typeM c x2) eqn : F. destruct (syn_eq_typeM m0_1 m0) eqn : I. 
      + inversion H; subst.
  apply IHx2 in F. rewrite F. apply syn_eq_type_MP in I. rewrite I. reflexivity.
+ inversion H.
+ inversion H.
+ inversion H.  Qed.
 


Lemma map_nil : Map inj_Type [] = [].
Proof. unfold Map. easy. Qed.







Lemma compo_does_nothing_on_inj_Type : forall m l, compo_n m (fun (i : nat) (C : PType) => PSubst_Type i PNat C)
     (inj_Type l) = inj_Type l.
Proof. intros m l. induction l.
- simpl. rewrite commu_prop_PBool. easy.
- simpl. rewrite commu_prop_PNat. easy.
- simpl. rewrite commu_prop_lam_type. simpl. rewrite IHl1. rewrite IHl2. easy. Qed.


Lemma compo_does_nothing_on_inj :  forall m t, compo_n m (fun (i : nat) (u : PTerm) => PSubst_Type_in_term i PNat u)
  (inj t) = inj t.
Proof. intros m t. induction t.
- simpl. rewrite commu_prop_true. easy.
- simpl. rewrite commu_prop_false. easy.
- simpl. rewrite prop_neg_subst. apply f_equal. easy.
- simpl. rewrite commu_prop_and. rewrite IHt1. rewrite IHt2. easy.
- simpl. rewrite commu_prop_var. easy.
- simpl. rewrite commu_prop_lam. rewrite IHt. rewrite compo_does_nothing_on_inj_Type. easy.
- simpl. rewrite commu_prop_app. rewrite IHt1. rewrite IHt2. easy. Qed.

Lemma subst_type_inj : forall M n',  PSubst_Type n' PNat (inj_Type M) = inj_Type M.
Proof. intros M. induction M; intros n'; simpl ; try reflexivity.
rewrite IHM1. rewrite IHM2. easy. Qed.




Lemma subst_inj : forall t n, PSubst_Type_in_term n PNat (inj t) = inj t.
Proof. intros t. induction t; intros n'; simpl in *; try reflexivity.
- rewrite IHt. easy.
- rewrite IHt1. rewrite IHt2. easy.
- rewrite IHt. rewrite subst_type_inj. easy.
- rewrite IHt1. rewrite IHt2. easy. Qed.




Lemma impossible_inj : forall n B, compo_n n (fun (i : nat) (C : PType) => PSubst_Type i PNat C) (PVar n) =
    inj_Type B -> False.
Proof. intros n. induction n; intros B.
- simpl. destruct B; simpl; try discriminate. 
- simpl. destruct (match n with
      | 0 => false
      | S m' => n =? m' end) eqn : E. apply match_impossible_arith in E. destruct E. 
rewrite max_var. destruct B; try discriminate. omega.

 Qed.

 


Lemma inj_inst_max : forall A n B, compo_n n (fun i C => PSubst_Type i PNat C) A = inj_Type B -> PSubst_Type n PNat A = A.
Proof. intros A. induction A; intros n' B H; simpl; try reflexivity.
- destruct (n =? n') eqn : E. Search "=?". rewrite Nat.eqb_eq in E. rewrite E in H. apply impossible_inj in H.
destruct H. easy.
- simpl in *. rewrite commu_prop_lam_type in H. assert (exists B1 B2, B = Ma B1 B2) as H1. destruct B; simpl in *;  try discriminate.
exists B1. exists B2. easy. destruct H1 as [B1 H1]. destruct H1 as [B2 H2]. rewrite IHA1 with n' B1.
rewrite IHA2 with n' B2. easy. rewrite H2 in H. inversion H. easy.  rewrite H2 in H. inversion H. easy. Qed.



Lemma max_inj_lam : forall n m A B, m >= n -> compo_n n (fun i C => PSubst_Type i PNat C) A  = inj_Type B
-> compo_n m (fun i C => PSubst_Type i PNat C) A  = inj_Type B.
Proof. intros n m. generalize dependent n. induction m; intros n A B H.
- simpl. apply le_zero in H. subst. simpl. easy.
- intros H1. assert (forall n m : nat, {n >= m} + {~ n >= m}) as H0. apply ge_dec.
specialize (H0 m n). destruct H0.
      * apply (IHm n A B) in g. simpl. assert (PSubst_Type m PNat A = A) as H2. apply inj_inst_max with B. easy. 
rewrite H2. easy. easy.
      * assert ( S m = n) as H2. apply le_not_succ. easy. easy. rewrite H2. easy.
Qed. 




Lemma max_lam : forall A B, trad_type A = B -> exists n, (compo_n n (fun i C => PSubst_Type i PNat C) A) = inj_Type B.
Proof. intros A. induction A; intros B H.
- assert (B = MBool) as H1; destruct B; try discriminate. reflexivity.
exists 0. simpl. reflexivity.
-  assert (B = MNat) as H1; destruct B; try discriminate. reflexivity.
exists 0. simpl. reflexivity.
- exists (S n). assert (B = MNat)  as H1; destruct B; try discriminate. reflexivity.
destruct n. simpl. reflexivity. simpl. destruct (n =? n) eqn: E. simpl. induction n. simpl.
reflexivity. simpl. apply IHn. simpl. reflexivity. inversion E. reflexivity.
rewrite Nat_as_OT.eqb_refl in E. discriminate.
- assert (B = Ma (trad_type A1) (trad_type A2)) as H1; destruct B; try discriminate.
simpl in H. eauto.
specialize (IHA1 B1). specialize (IHA2 B2). inversion H. apply IHA1 in H2. apply IHA2 in H3.
destruct H2 as [n H2]. destruct H3 as [n' H3]. apply max_inj_lam with _ (max n n') _ _ in H2.
apply max_inj_lam with _ (max n n') _ _ in H3. exists (max n n'). simpl. inversion H. rewrite H4. rewrite H5. simpl. 
rewrite commu_prop_lam_type. rewrite H2. rewrite H3. easy.
apply max_lemma. apply max_lemma. 
Qed.





Lemma P_Subst_Type_Prop  : forall p n m, PSubst_Type n PNat (PSubst_Type m PNat p) = PSubst_Type m PNat (PSubst_Type n PNat p).
Proof. intros p; induction p; intros i i'; simpl in *; try reflexivity.
- destruct (n =? i') eqn : E. destruct (n=? i) eqn : G; simpl; try reflexivity. rewrite E. reflexivity.
destruct (n=? i) eqn : G; simpl; try reflexivity. rewrite G. reflexivity. 
rewrite G. rewrite E. reflexivity.
- rewrite IHp1. rewrite IHp2. easy. Qed.




Lemma PSubst_Type_diff : 
forall m n, n <> m -> forall A,
  PSubst_Type m PNat (PSubst_Type n PNat A) = PSubst_Type n PNat A ->
  PSubst_Type m PNat A = A.
Proof. intros m n H. intros A. induction A; simpl in *; try easy; intros H1.
- destruct (n0 =? m) eqn : E. apply beq_nat_true in E. subst. destruct  (m =? n) eqn : F. 
apply beq_nat_true in F. omega. simpl in H1.   rewrite Nat.eqb_refl in H1. easy. easy.
- rewrite IHA1. rewrite IHA2. easy. inversion H1; subst. rewrite H3. rewrite H3.
easy. inversion H1; subst. rewrite H2. rewrite H2. easy.
Qed.



Lemma PSubst_Type_in_term_diff :
  forall m n, n <> m -> forall x,
  PSubst_Type_in_term m PNat (PSubst_Type_in_term n PNat x) = PSubst_Type_in_term n PNat x ->
  PSubst_Type_in_term m PNat x = x.
Proof. intros m n H. intros x. induction x; simpl in *; try easy; intros H1.
- rewrite IHx. easy. inversion H1; subst. rewrite H2. apply H2.
- rewrite IHx1. rewrite IHx2. easy. inversion H1; subst. rewrite H3. rewrite H3.
easy. inversion H1; subst. rewrite H2. rewrite H2. easy.
- rewrite IHx. inversion H1. assert (PSubst_Type m PNat p = p) as H4. eapply 
PSubst_Type_diff. apply H. apply H2. rewrite H4. easy. inversion H1.  rewrite H3.
apply H3.
- rewrite IHx1. rewrite IHx2. easy. inversion H1; subst. rewrite H3. rewrite H3.
easy. inversion H1; subst. rewrite H2. rewrite H2. easy.
Qed.


Lemma PSubst_Type_inj : forall m p l, p = inj_Type l -> PSubst_Type m PNat p = p.
Proof.
intros m p. induction p; intros l H; simpl in *; try reflexivity.
- destruct l; try discriminate.
- assert (exists l1 l2, l = l1 ---> l2). destruct l; try discriminate.
exists l1. exists l2. easy. destruct H0 as [l1 H0]. destruct H0 as [l2 H0].
rewrite H0 in H. inversion H. rewrite <- H2. rewrite <- H3.
rewrite IHp1 with l1. rewrite IHp2 with l2. easy. easy. easy. Qed. 

Lemma PSubst_Type_in_term_inj :
  forall m x t, x = inj t -> PSubst_Type_in_term m PNat x = x.
Proof. intros m x; induction x; intros t H; simpl in * ; try reflexivity.
-  assert (exists t', PNeg x = inj (MNeg t')) as H1.
destruct t; simpl in * ; try discriminate. exists t. apply H. destruct H1 as [t' H1].
simpl in H1. assert (x = inj t') as H2. inversion H1. easy. rewrite H2.
specialize (IHx t'). apply IHx in H2. inversion H1. rewrite H3 in H2. rewrite H2. easy.
- assert (exists t' t'', (PAnd x1 x2) = inj (MAnd t' t'')) as H1.
destruct t; simpl in * ; try discriminate. exists t1. exists t2.  apply H. destruct H1 as [t1 H1].
destruct H1 as [t2 H2].
simpl in H2. assert (x1 = inj t1) as H3. inversion H2. easy. rewrite H2.
assert (x2 = inj t2) as H4. inversion H2. easy. rewrite H4.
specialize (IHx1 t1). apply IHx1 in H3. specialize (IHx2 t2). apply IHx2 in H4. 
inversion H2. rewrite H1 in H3. rewrite H5 in H4. rewrite H3. rewrite H4. easy.
- assert (exists A t, (PLam p x) = inj (MLam A t)) as H1. 
destruct t; simpl in * ; try discriminate. exists m0. exists t. apply H.
destruct H1 as [A H1]. destruct H1 as [t' H1]. simpl in H1. inversion H1.
rewrite <- H2. rewrite <- H3. apply IHx in H3. rewrite H3. rewrite PSubst_Type_inj with m p A.
easy. easy.
- assert (exists t' t'', (PApp x1 x2) = inj (MApp t' t'')) as H1.
destruct t; simpl in * ; try discriminate. exists t1. exists t2.  apply H. destruct H1 as [t1 H1].
destruct H1 as [t2 H2].
simpl in H2. assert (x1 = inj t1) as H3. inversion H2. easy. rewrite H2.
assert (x2 = inj t2) as H4. inversion H2. easy. rewrite H4.
specialize (IHx1 t1). apply IHx1 in H3. specialize (IHx2 t2). apply IHx2 in H4. 
inversion H2. rewrite H1 in H3. rewrite H5 in H4. rewrite H3. rewrite H4. easy.
Qed.


Lemma compo_n_inj : forall n m, n <= m ->
                    forall x t,
                      compo_n n (fun i u => PSubst_Type_in_term i PNat u) x = inj t ->
                      PSubst_Type_in_term m PNat x = x.
Proof.
  induction n as [ |n IHn].
  - simpl. intros m _. now apply PSubst_Type_in_term_inj.
  - simpl. intros m Hm x t H. apply (IHn m) in H.
    + apply (PSubst_Type_in_term_diff m n); auto.
      omega.
    + omega.
Qed.

Lemma max_inj : forall t m n, n <= m ->
                forall x, compo_n n (fun i u => PSubst_Type_in_term i PNat u) x = inj t ->
                          compo_n m (fun i u => PSubst_Type_in_term i PNat u) x = inj t.
Proof.
  intro t. induction 1 as [ |m H IH]; auto.
  intros x Hn. simpl. rewrite IH; auto.
  now rewrite (compo_n_inj _ _ H _ _ Hn).
Qed.



Lemma reverse_inj : forall x t, trad_term x = t -> exists n, (compo_n n (fun i u => PSubst_Type_in_term i PNat u) x) = inj t.
Proof. intros x t H. generalize dependent x. induction t; intros x H.
- exists 0; simpl. assert (x = PTrue) as H2. destruct x; 
simpl in  *; try discriminate. reflexivity. apply H2.
- exists 0; simpl. assert (x = PFalse) as H2. destruct x; 
simpl in  *; try discriminate. easy. apply H2.
- assert (exists x', x = PNeg x').  destruct x; 
simpl in  *; try discriminate. exists x. reflexivity.
destruct H0 as [x' H0]. rewrite H0 in H. simpl in H.
specialize (IHt x'). inversion H. apply IHt in H2. inversion H. destruct H2 as [n H2].
exists n. simpl. inversion H0 as [H4]. generalize dependent x'.
induction n; intros.  
      * simpl in *. rewrite H2. rewrite trad_inv. reflexivity.
      * simpl in *. rewrite compo_neg. apply f_equal. rewrite H3. apply H2.
- assert (exists x1 x2, x = PAnd x1 x2) as H0. destruct x; 
simpl in  *; try discriminate. exists x1. exists x2. easy. destruct H0 as [x1 H0]. destruct H0 as [x2 H0].
subst. simpl in *. specialize (IHt1 x1). specialize (IHt2 x2). inversion H. intuition.
destruct H0 as [n H0]. destruct H3 as [n' H3]. exists (max n n'). apply max_inj with _ (max n n')_ _ in H0.
apply max_inj with _ (max n n')_ _ in H3. rewrite <- H1 in H0. rewrite <- H2 in H3.
induction (max n n'). intros.
    * simpl in *. rewrite <- H0. rewrite <- H3. easy.
    * simpl in *. rewrite compo_and.  rewrite H3. rewrite H0. easy.
    * apply max_lemma.
    * apply max_lemma.
- exists 0. simpl. assert (x = PTermVar n) as H1. destruct x; 
simpl in  *; try discriminate. inversion H. easy. 
rewrite H1. simpl. easy.
- assert (exists A x', x = PLam A x') as H0. destruct x; 
simpl in  *; try discriminate. exists p. exists x. easy.
destruct H0 as [A H0]. destruct H0 as [x' H0]. rewrite H0. simpl in *. specialize (IHt x').
rewrite H0 in H. inversion H. apply IHt in H3. destruct H3 as [n H3]. apply max_lam in H2.
destruct H2 as [n' H2]. exists (max n n').
rewrite commu_prop_lam. apply max_inj with _ (max n n')_ _ in H3. apply max_inj_lam with _ (max n n')_ _ in H2.
rewrite H2. rewrite H3. inversion H. rewrite H4. rewrite H5. easy.
apply max_lemma. apply max_lemma.
- assert (exists x1 x2, x = PApp x1 x2) as H0. destruct x; 
simpl in  *; try discriminate. exists x1. exists x2. easy. destruct H0 as [x1 H0]. destruct H0 as [x2 H0].
subst. simpl in *. specialize (IHt1 x1). specialize (IHt2 x2). inversion H. intuition.
destruct H0 as [n H0]. destruct H3 as [n' H3]. exists (max n n'). apply max_inj with _ (max n n')_ _ in H0.
apply max_inj with _ (max n n')_ _ in H3. rewrite <- H1 in H0. rewrite <- H2 in H3.
induction (max n n'). intros.
    * simpl in *. rewrite <- H0. rewrite <- H3. easy.
    * simpl in *. rewrite compo_app.  rewrite H3. rewrite H0. easy.
    * apply max_lemma.
    * apply max_lemma.

Qed.





Lemma valid_compo : forall c x, validP c x -> forall n, validP c (compo_n n (fun i x => PSubst_Type_in_term i PNat x) x).
Proof. intros c x H n. 
 induction n as [ | n].
- simpl in *. apply H.
- simpl in *. apply SemPSubst with _ _ n PNat in IHn. rewrite commu_compo_PSubstTerm in IHn. apply IHn. inversion H; try easy. Qed.   




Theorem trad_is_sound: forall c t, context_okP c -> validM (trad_set c) t -> validP c (inj t).
Proof. intros c t H0 H. remember (trad_set c) as d. induction H.
- simpl. apply SemPTrue. easy.
- apply IHvalidM in Heqd. apply conv_Valid with (t:=t) (u:=u)  (c:= c) in H1. destruct H1 as [H1 H3].
apply H1 in Heqd. easy. apply H0.
- subst. unfold trad_set in *. apply map_spec2 in H1. destruct H1 as [x' [H2 H3]].
symmetry in H3. apply reverse_inj in H3. destruct H3 as [n H3]. rewrite <- H3. eapply valid_compo. eapply SemPAssume. 
apply PFalse. apply H0. apply H2. 
 - assert (c0 = trad_set c) as Heqd'. easy. apply IHvalidM1 in Heqd. apply IHvalidM2 in Heqd'.
simpl. apply SemPAnd. easy. easy. easy.
Qed.




